#ifndef __INTERFACE__
#define __INTERFACE__

#include "visite.h"

#define LARG 600
#define BORD 20

typedef struct {
	int x;
	int y;
} Coordonnee;

void affiche_ville(const Carte *carte, int indice_ville);
void affiche_villes(const Carte *carte);
void affiche_chemin(const Ville *ville_depart, const Ville *ville_arrive);
void affiche_chemins(const Visite *visite, const Carte *carte);
void afficher_visite(const Visite *visite, const Carte *carte);
void creer_fenetre(const Coordonnee *coord);

#endif
