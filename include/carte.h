#ifndef __CARTE__
#define __CARTE__

/* Implémentation des bibliotèques requises	*/
	#include "ville.h"

	/*	Déclaration des types & variables	*/
	typedef struct {
		Ville *villes;
		int nb_villes;
	} Carte;

	/* Déclaration des fonctions	*/

		/*	Fonction d'initialisation de Carte. Elle prend en argument 
			une Carte non initialisée (c'est-à-dire n'ayant pas effectuée
			de malloc) et un nombre de villes maximum (>0)	*/
	void init_carte(Carte *carte, int nb_villes);

		/*	Fonction permet à l'utilisateur de saisir le nom et les
			coordonnées de toute les villes */
	void saisir_carte(Carte *carte);

		/* Realloc */
	void ajouter_ville(Carte *carte, const Ville *ville);

		/*	Libère la mémoire du champs Ville *villes d'une Carte */
	void supprimer_carte(Carte *carte);

		/*	Fonction recopiant les données d'un fichier ayant comme format
			celui de exporter_fichier_carte	et recreer une Carte à partir
			de celles-ci. Si une carte a été généré mais que le fichier
			n'est pas valide, celle-ci sera automatiquement libérée en 
			mémoire. Retourne 0 si Erreur, 1 sinon	*/
	int importer_fichier_carte(FILE *f, Carte *carte);

		/*	Exporte les données d'une Carte dans une sortie selon le format
			suivant :
				- CARTE
				- nb_villes
				- nom_ville pos_x_ville pos_y_ville
				- ...
				- EOF	*/
	void exporter_fichier_carte(FILE *f, const Carte *carte);

		/* Fonction permettant de creer une carte aléatoire de nb_villes
			avec la définiton de leur borne (borne_inf et borne_sup) */
	void creer_carte_alea(Carte *carte, int nb_villes, int borne_inf, int borne_sup);
#endif
