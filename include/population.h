#ifndef __POPULATION__
#define __POPULATION__

#include "visite.h"

typedef struct {
	int nb_visites;
	int nb_generation;
	int nb_meilleur;
	int nb_mutation;
	int nb_reproduction;
} Evolution;

typedef struct {
	Carte carte;
	Visite *visites;
	Evolution evolution;
} Population;

void init_evolution(Evolution *evolution, int nb_visites, int nb_generation, int nb_meilleur, int nb_mutation, int nb_reproduction);
void init_population(Population *population, const Evolution *evolution, const Carte *carte);
Visite* init_tab_visite(int nb_visites, int nb_villes);
void supprimer_tab_visite(Visite *visites, int taille);
void trier_population(Population *pop);                                                                                                      
void lancer_simulation(Population *pop);
#endif
