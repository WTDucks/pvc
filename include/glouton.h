#ifndef __GLOUTON__
#define __GLOUTON__

#include "visite.h"

void trouver_ville_proche(Visite *visite, const Carte * carte, const int ville_actuelle);
void glouton(const Carte *carte, Visite *visite);

#endif
