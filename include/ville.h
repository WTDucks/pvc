#ifndef __VILLE__
#define __VILLE__

/*	Implémentation des bibliotèques requises	*/
	#include <stdio.h>

/* Déclaration des types & variables */
	typedef struct {
		char *nom;
		int pos_x; 
		int pos_y;
	} Ville;

/* Déclaration des fonctions */

	void init_ville(Ville *ville);

	/*	La fonction modifie, via un pointeur, une ville pour y mettre un
		nom, une position pos_x et pos_y.	*/
	void creer_ville(Ville *ville_vide, char *nom, int pos_x, int pos_y);

	/*	Libère la mémoire du champs char *nom d'une Ville*/
	void supprimer_ville(Ville *ville);

	/*	L'utlisateur rentre sur l'entrée standart le noms et les coordonnées
		d'une Ville de son choix afin d'etre appliquée sur la Ville passée
		en paramètre. Ce fonction retourne 0 si l'entrée est invalide,
		1 sinon	*/
	int saisir_ville(Ville *ville);

	/*	Fonction qui récupère le nom et les coordonées d'une Ville à partir
		d'un FILE préalablement OUVERT. Ville *ville_vide est un pointeur
		de Ville qui permet de récupérer les informations précédantes
		Retourne 0 si lecture incorrecte. 1 sinon	*/
	int importer_fichier_ville(FILE *f, Ville *ville_vide);

	/*	Fonction qui écrit les données d'une Ville dans un FILE 
		passé en paramètres	*/
	void exporter_fichier_ville(FILE *f, const Ville *v);

	void int_alea(int *x, int borne_inf, int borne_sup);

	void nom_aleatoire(char *nom, int x, int y);
	
	/*	Fonction qui modifie les données de ville de manière aléatoire	*/
	void creer_ville_alea(Ville *ville, int borne_inf, int borne_sup);
#endif
