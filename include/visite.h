#ifndef __VISITE__
#define __VISITE__

/*	Implémentation des headers requis	*/
	#include "carte.h"

/* Déclaration des types & variables */
	typedef struct {
		int *entree_ville;
		int nb_villes;
		int distance_totale;
	} Visite;

/* Déclaration des fonctions */

	/* Fonction qui effectue un malloc sur int *entree_ville et 
		initiallise nb_ville/distance_totale	*/
	void init_visite(Visite *visite, int nb_villes);

	/* Fonction qui utlise free() pour libérer entree_ville	*/
	void supprimer_visite(Visite *visite);

	/* Fonction classique, dite de swap, entre deux visites	*/
	void echange_visite(Visite *visite, int indice_1, int indice_2);

	/* Retourne 1 si x trouvé, 0 sinon */
	int recherche_visite(const Visite *visite, int x);

	/* Copie via memecpy */
	void copier_visite(Visite *dest, const Visite *src);

	/*	Fonction recopiant les données d'un fichier ayant comme format
		celui de exporter_fichier_visite	et creer une visite à partir
		de celles-ci. Si une visite a été généré mais que le fichier
		n'est pas valide, celle-ci sera automatiquement libérée en 
		mémoire. Retourne 0 si Erreur, 1 sinon	*/
	int importer_fichier_visite(FILE *f, Visite *visite);

	/* Exporte les données d'une Carte dans une sortie selon le format
		suivant :
			- VISITE
			- nb_villes
			- indice de la ville de départ
			- ...
			- indice de la ville d'arrivée
			- EOF	*/
	void exporter_fichier_visite(FILE *f, const Visite *visite);

	void calcul_distance(const Carte *carte, int *distance, int indice_depart, int indice_arrivee);

	void calcul_distance_visite(Visite *visite, const Carte *carte);

	/*	Fonction permettant de creer une visite aléatoire à partir
		d'une Carte données */
	void creer_visite_alea(Visite *visite, const Carte *carte);

	/* Inverse deux valeurs du tableau de int */
	void mutation(Visite *visite);

	/* Fusionne deux visites pour en former une nouvelle */
	void reproduction(Visite *enfant, const Visite *pere, const Visite *mere, const Carte *carte);
#endif
