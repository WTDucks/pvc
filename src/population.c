#include "../include/population.h"

/* Implémentation des bibliotèques standard requises	*/
	#include <stdlib.h>
	#include <string.h>
	#include <assert.h>

void init_evolution(Evolution *evolution, int nb_visites, int nb_generation, int nb_meilleur, int nb_mutation, int nb_reproduction) {
	assert(evolution);
	assert(nb_visites > 0);
	assert(nb_meilleur > 0);

	evolution->nb_visites = nb_visites;
	evolution->nb_generation = nb_generation;
	evolution->nb_meilleur = nb_meilleur;
	evolution->nb_mutation = nb_mutation;
	evolution->nb_reproduction = nb_reproduction;
}
		
void init_population(Population *population, const Evolution *evolution, const Carte *carte) {
	assert(population);
	assert(evolution);
	assert(carte);
	assert(evolution->nb_visites > 0);

	population->visites = (Visite*)malloc(sizeof(Visite) * evolution->nb_visites);
	if (NULL == population->visites) {
		perror("init_population : malloc error");
		exit(EXIT_FAILURE);
	}
	population->evolution = *evolution;
	population->carte = *carte;
	for (int i = 0; i < evolution->nb_visites; ++i)
		creer_visite_alea(&(population->visites[i]), carte);
}

Visite* init_tab_visite(int nb_visites, int nb_villes) {
	assert(nb_visites > 0);

	Visite *tab = (Visite *) malloc(nb_visites * sizeof(Visite));
	if(tab == NULL){
		printf("erreur\n");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < nb_visites; ++i)
		init_visite(&(tab[i]), nb_villes);
	return tab;
}

void supprimer_tab_visite(Visite *visites, int taille) {
	assert(visites);
	for (int i = taille - 1; i >= 0; --i) {
		supprimer_visite(&(visites[i]));
	}
	visites = NULL;
}

static int compare_int(const void *p1, const void *p2) {
	return ( ((Visite*)p1)->distance_totale - ((Visite*)p2)->distance_totale );
}

void trier_population(Population *pop) {
	qsort(pop->visites, pop->evolution.nb_visites, sizeof(Visite), compare_int);
}

void lancer_simulation(Population *pop) {
	int indice;
	Visite *nouveau = init_tab_visite(pop->evolution.nb_visites, pop->carte.nb_villes);
	assert(pop);

	/* Sauvegarde des meilleurs */
	for (indice = 0; indice < pop->evolution.nb_meilleur; ++indice){
		copier_visite(&(nouveau[indice]), &(pop->visites[indice]));
	}

	/* Reproduction */
	for (int k = 0; k < pop->evolution.nb_reproduction; ++k) {
		reproduction(&(nouveau[indice]), &(pop->visites[indice]), &(pop->visites[indice+1]), &(pop->carte));
		++indice;
	}
	/* Mutation */
	for (int l = 0; l < pop->evolution.nb_mutation; ++l) {
		mutation(&(pop->visites[indice]));
		copier_visite(&(nouveau[indice]), &(pop->visites[indice]));
		++indice;
	}

	/* Ajout de Visite aléatoire */
	for (; indice < pop->evolution.nb_visites; ++indice) {
		supprimer_visite(&(nouveau[indice]));
		creer_visite_alea(&(nouveau[indice]), &(pop->carte));
	}

	/* Recopiage de nouveau dans pop */
	for(int ok = 0; ok < pop->evolution.nb_visites; ++ok){
		copier_visite(&(pop->visites[ok]), &(nouveau[ok])); 
	}

	trier_population(pop);
}
