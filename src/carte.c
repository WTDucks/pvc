/* HEADER	*/
	#include "../include/carte.h"

/* Implémentation des bibliotèques requises	*/
	#include <stdlib.h>
	#include <assert.h>	
	#include <string.h>

void init_carte(Carte *carte, int nb_villes) {
	assert(carte);
	assert(nb_villes > 0);

	carte->nb_villes = nb_villes;

	carte->villes = (Ville *) malloc(nb_villes*sizeof(Ville));
	if(carte->villes == NULL){
		perror("erreur malloc");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < nb_villes; ++i) {
		init_ville(&(carte->villes[i]));
	}
}

void saisir_carte(Carte *carte) {
	int i;
	assert(carte);

	for (i = 0; i < carte->nb_villes; ++i) {
		saisir_ville(&(carte->villes[i]));
	}
}

void ajouter_ville(Carte *carte, const Ville *ville) {
	assert(carte);
	assert(ville);

	carte->villes = (Ville *) realloc(carte->villes, (carte->nb_villes + 1) * sizeof(Ville));
	if (carte->villes == NULL) {
		perror("ajouter_ville : erreur de realloc");
		exit(EXIT_FAILURE);
	}
	carte->nb_villes += 1;
	carte->villes[carte->nb_villes-1] = *ville;
}

void supprimer_carte(Carte *carte) {
	int i;
	assert(carte);

	for (i = carte->nb_villes; i > 0; --i)
		supprimer_ville(&(carte->villes[i]));

	carte->villes = NULL;
	carte->nb_villes = 0;
}

int importer_fichier_carte(FILE *f, Carte *carte) {
	int i;
	int nb_villes;
	int ret;
	char *est_carte = NULL;
	assert(carte);
	assert(f);

	est_carte = (char*) malloc(sizeof(char));
	ret = fscanf(f, "%s", est_carte);
	if(ret != 1 || strcmp(est_carte, "CARTE") != 0){
		free(est_carte);
		return 0;
	}
	free(est_carte);

	ret = fscanf(f, "%d", &nb_villes);
	if(ret != 1 || nb_villes < 1)
		return 0;

	init_carte(carte, nb_villes);

	ret = 1;
	for (i = 0; i < nb_villes && ret != 0; ++i)
		ret = importer_fichier_ville(f, &(carte->villes[i]));

	if(ret == 0 || i != nb_villes || feof(f) == 0){
		carte->nb_villes = i; /* On rajuste le nombre de ville lue */
		supprimer_carte(carte); /* Qui n'est plus vide dans ce cas */
		return 0;
	}
	return 1;
}

void exporter_fichier_carte(FILE *f, const Carte *carte) {
	int i;
	assert(f);
	assert(carte);
	assert(carte->villes);

	fprintf(f, "CARTE\n");
	fprintf(f, "%d\n",carte->nb_villes);
	for(i = 0; i < carte->nb_villes; ++i)
        exporter_fichier_ville(f,&(carte->villes[i]));
}

void creer_carte_alea(Carte *carte, int nb_villes, int borne_inf, int borne_sup){
	int i;
	assert(carte);
	assert(nb_villes > 1);
	assert(borne_inf >= 0);
	assert(borne_sup > 0);

	init_carte(carte, nb_villes);
	for(i = 0; i < nb_villes; ++i)
		creer_ville_alea(&(carte->villes[i]), borne_inf, borne_sup);
}
