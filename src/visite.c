#include "../include/visite.h"

/* Implémentation des bibliotèques requises	*/
	#include <stdlib.h>
	#include <assert.h>	
	#include <string.h>
	

void init_visite(Visite *visite, int nb_villes) {
	assert(visite);
	assert(nb_villes > 0);

	visite->nb_villes = nb_villes;
	visite->distance_totale = 0;
	visite->entree_ville = NULL;
	visite->entree_ville = (int*) malloc(sizeof(int)*nb_villes);
	if (visite->entree_ville == NULL) {
		perror("init_visite : malloc error");
		exit(EXIT_FAILURE);
	}
}

void supprimer_visite(Visite *visite) {
	assert(visite);
	
	free(visite->entree_ville);
	visite->entree_ville = NULL;
	visite->nb_villes = 0;
	visite->distance_totale = 0;
	visite = NULL;
}

void echange_visite(Visite *visite, int indice_1, int indice_2) {
	int tab_tmp;
	assert(visite);
	assert(indice_1 >= 0);
	assert(indice_2 >= 0);

	tab_tmp = visite->entree_ville[indice_1];
	visite->entree_ville[indice_1] = visite->entree_ville[indice_2];
	visite->entree_ville[indice_2] = tab_tmp;
}

int recherche_visite(const Visite *visite, int x) {
	assert(visite);
	assert(x >= 0 && x < visite->nb_villes);
	for(int i = 0; i < visite->nb_villes; ++i)
		if(visite->entree_ville[i] == x)
			return 1;
	return 0;
}

void copier_visite(Visite *dest, const Visite *src) {
	assert(dest);
	assert(src);
	dest->nb_villes = src->nb_villes;
	dest->distance_totale = src->distance_totale;
	memcpy(dest, src, src->nb_villes);
}

int importer_fichier_visite(FILE *f, Visite *visite) {
	int i = 0;
	int nb_villes;
	int ret;
	char *est_visite = NULL;
	assert(visite);
	assert(f);

	est_visite = (char*) malloc(sizeof(char));
	if (est_visite == NULL) {
		perror("importer_fichier_visite : malloc error");
		exit(EXIT_FAILURE);
	}

	ret = fscanf(f, "%s", est_visite);
	if (ret != 1 || strcmp(est_visite, "VISITE") != 0) {
		free(est_visite);
		return 0;
	}
	free(est_visite);

	ret = fscanf(f, "%d", &nb_villes);
	if (ret != 1 || nb_villes < 2)
		return 0;

	init_visite(visite, nb_villes);
	while (fscanf(f, "%d", &(visite->entree_ville[i])) != EOF)
		i += 1;

	if (nb_villes != i || feof(f) == 0) {
		supprimer_visite(visite);
		return 0;
	}
	return 1;
}

void exporter_fichier_visite(FILE *f, const Visite *visite) {
	int i;
	assert(f);
	assert(visite);
	assert(visite->entree_ville);
	assert(visite->nb_villes > 0);

	fprintf(f, "VISITE\n");
	fprintf(f, "Nombres de Villes : %d\n", visite->nb_villes);
	fprintf(f, "Distance Totale : %d\n", visite->distance_totale);
	for (i = 0; i < visite->nb_villes; ++i)
		fprintf(f, "%d\n", visite->entree_ville[i]);
}

void calcul_distance(const Carte *carte, int *distance, int indice_depart, int indice_arrivee) {
	int a;
	int b;
	assert(carte);
	assert(distance);
	assert(indice_depart >= 0 && indice_depart <= carte->nb_villes-1);

	/* Apllication du théorème de Pythagore c² = a² + b² */
	a = carte->villes[indice_depart].pos_x - carte->villes[indice_arrivee].pos_x;
	a *= a;

	b = carte->villes[indice_depart].pos_y - carte->villes[indice_arrivee].pos_y;
	b *= b;

	*distance = a + b;
}

void calcul_distance_visite(Visite *visite, const Carte *carte) {
	int distance_totale = 0;
	assert(visite);
	assert(carte);
	for (int i = 0; i < visite->nb_villes - 1; ++i){
		calcul_distance(carte, &distance_totale, visite->entree_ville[i], visite->entree_ville[i+1]);
		visite->distance_totale += distance_totale;
   }
}

void creer_visite_alea(Visite *visite, const Carte *carte) {
	int i,j, rand_int;
	assert(carte);
	assert(visite);

	init_visite(visite, carte->nb_villes);

	for (i = 0; i < visite->nb_villes; ++i)
        visite->entree_ville[i] = i;

    if (carte->nb_villes > 3) {
		for (i = 1, j = 2; j != visite->nb_villes-2; ++j) {
			rand_int = i + rand() % (visite->nb_villes-j);
			echange_visite(visite, rand_int, visite->nb_villes-j);
		}
	}
	calcul_distance_visite(visite, carte);
}

void mutation(Visite *visite) {
	assert(visite);
	assert(visite->nb_villes > 3);
	int x,y;
	int_alea(&x, 1, visite->nb_villes - 2);
	int_alea(&y, 1, visite->nb_villes - 2);
	if(x == y)
		mutation(visite);
	echange_visite(visite, x, y);
}

void reproduction(Visite *enfant, const Visite *pere, const Visite *mere, const Carte *carte) {
	int indice_enfant, indice_parents;

	init_visite(enfant, pere->nb_villes);
	enfant->entree_ville[0] = 0;
	enfant->entree_ville[enfant->nb_villes-1] = enfant->nb_villes-1;
	for(indice_enfant = 1, indice_parents = 1; indice_enfant < enfant->nb_villes - 1; indice_parents++) {
		if(recherche_visite((const Visite *)enfant, pere->entree_ville[indice_parents]) == 0){
			enfant->entree_ville[indice_enfant] = pere->entree_ville[indice_parents];
			indice_enfant++;
		}
		if(indice_enfant < enfant->nb_villes - 1) {
			if(recherche_visite((const Visite *)enfant, mere->entree_ville[indice_parents]) == 0) {
				enfant->entree_ville[indice_enfant] = mere->entree_ville[indice_parents];
				indice_enfant++;
			}
		}
	}
	calcul_distance_visite(enfant, carte);
}
