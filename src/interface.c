#include "../include/interface.h"
#include <MLV/MLV_all.h>
#include <assert.h>

void affiche_ville(const Carte *carte, int indice_ville) {
	assert(carte != NULL);
	assert(indice_ville >= 0);
	assert(carte->nb_villes > 0);

	if (indice_ville == 0) {
		MLV_draw_filled_circle(	
			(carte->villes[indice_ville].pos_x),
			(carte->villes[indice_ville].pos_y),
			4,
			MLV_COLOR_GREEN
		);
	}

	else if (indice_ville == carte->nb_villes - 1) {
		MLV_draw_filled_circle(
			(carte->villes[indice_ville].pos_x),
			(carte->villes[indice_ville].pos_y),
			4,
			MLV_COLOR_RED
		);
	}

	else{
		MLV_draw_filled_circle(
			(carte->villes[indice_ville].pos_x),
			(carte->villes[indice_ville].pos_y),
			4,
			MLV_COLOR_WHITE
		);
	}
	MLV_actualise_window();
}

void affiche_villes(const Carte *carte){
	assert(carte != NULL);
	for (int i = 0; i < carte->nb_villes; ++i)
		affiche_ville(carte, i);
	MLV_actualise_window();
}

void affiche_chemin(const Ville *ville_depart, const Ville *ville_arrive) {

	assert(ville_depart != NULL);
	assert(ville_arrive != NULL);
	MLV_draw_line(
		ville_depart->pos_x,
		ville_depart->pos_y,
		ville_arrive->pos_x,
		ville_arrive->pos_y,
		MLV_COLOR_GRAY82
	);
}

void affiche_chemins(const Visite *visite, const Carte *carte) {
	assert(visite != NULL);
	assert(carte != NULL);
	assert(visite->nb_villes == carte->nb_villes);

	for (int i = 0; i < visite->nb_villes - 1; ++i) {

		affiche_chemin(
			&(carte->villes[visite->entree_ville[i]]),
			&(carte->villes[visite->entree_ville[i+1]])
		);
	}
}

void afficher_visite(const Visite *visite, const Carte *carte) {
	assert(visite != NULL);
	assert(carte != NULL);

	MLV_draw_filled_rectangle(0,0,LARG,LARG,MLV_COLOR_BLACK);
	affiche_chemins(visite, carte);
	affiche_villes(carte);
}

void creer_fenetre(const Coordonnee *coord) {
	assert(coord != NULL);

	MLV_create_window(
		"Problème Du Voyageur de Commerce", 
		"Problème du Voyageur de Commerce", 
		coord->x, coord->y
	);
	MLV_actualise_window();
}
