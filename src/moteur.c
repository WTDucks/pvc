#include "../include/glouton.h"

/* Implémentation des bibliotèques standard requises	*/
	#include <stdlib.h>
	#include <assert.h>

void trouver_ville_proche(Visite *visite, const Carte * carte, const int ville_actuelle) {
	int t_indice;
	int t_distance;
	int meilleur_distance = 0;
	int meilleur_ville = 0;
	assert(carte);
	assert(visite);
	assert(ville_actuelle < carte->nb_villes - 2);

	for (int i = ville_actuelle+1; i < carte->nb_villes - 1; ++i) {
		calcul_distance(carte, &t_distance, visite->entree_ville[ville_actuelle], visite->entree_ville[i]);
		if (meilleur_distance > t_distance || meilleur_distance == 0) {
			meilleur_distance = t_distance;
			meilleur_ville = i;
		}
	}

	t_indice = visite->entree_ville[ville_actuelle +1];
	visite->entree_ville[ville_actuelle +1] = visite->entree_ville[meilleur_ville];
	visite->entree_ville[meilleur_ville] = t_indice;
}

void glouton(const Carte *carte, Visite *visite) {
	assert(carte);
	assert(visite);

	init_visite(visite, carte->nb_villes);
	for (int j = 0; j < visite->nb_villes; ++j)
        visite->entree_ville[j] = j;

	for (int i = 0; i < carte->nb_villes - 2; ++i) {
		trouver_ville_proche(visite, carte, i);
	}
	calcul_distance_visite(visite, carte);
}
