/* HEADER	*/
	#include "../include/ville.h"

/* Implémentation des bibliotèques standard requises	*/
	#include <stdlib.h>
	#include <assert.h>	
	#include <string.h>

void init_ville(Ville *ville) {
	assert(ville);
	ville->nom = (char*) malloc(sizeof(char));
	if (NULL == ville->nom) {
		perror("creer_ville : malloc error");
		exit(EXIT_FAILURE);
	}
}

void creer_ville(Ville *ville_vide, char *nom, int pos_x, int pos_y) {
	assert(ville_vide);
	assert(nom);
	assert(pos_x >= 0);
	assert(pos_y >= 0);
	
	init_ville(ville_vide);
	strcpy(ville_vide->nom, nom);
	ville_vide->pos_x = pos_x;
	ville_vide->pos_y = pos_y;
}

void supprimer_ville(Ville *ville) {
	assert(ville);

	free(ville->nom);
	ville->nom = NULL;
}

int saisir_ville(Ville *ville) {
	int ret_1;
	int ret_2 = 0;
	char *nom_ville;
	int pos_x, pos_y;


	nom_ville = (char*) malloc(sizeof(char));
	if (NULL == nom_ville) {
		perror("saisir_ville : malloc error");
		exit(EXIT_FAILURE);
	}

	ret_1 = scanf("%s",nom_ville);
	ret_2 += scanf("%d",&pos_x);
	ret_2 += scanf("%d",&pos_y);

	if (ret_1 == 1 && ret_2 == 2) {
		creer_ville(ville, nom_ville, pos_x, pos_y);
		free(nom_ville);
		return 1;
	}
	free(nom_ville);
	return 0;
}

int importer_fichier_ville(FILE *f, Ville *ville_vide) {
	char *nom_ville;
	int pos_x, pos_y, ret;
	assert(f);
	assert(ville_vide);

	nom_ville = (char*) malloc(sizeof(char));
	if (NULL == nom_ville)
		exit(EXIT_FAILURE);

	ret = fscanf(f,"%s %d %d ", nom_ville, &pos_x, &pos_y);
	if (ret != 3)
		return 0;

	creer_ville(ville_vide, nom_ville, pos_x, pos_y);
	free(nom_ville);
	return 1;
}

void exporter_fichier_ville(FILE *f, const Ville *v) {
	assert(f);
	assert(v);


	fprintf(f, "%s %d %d\n", v->nom, v->pos_x, v->pos_y);
}

void int_alea(int *x, int borne_inf, int borne_sup) {
	assert(x);
	*x = borne_inf + (rand() % (int)(borne_sup - borne_inf + 1));
}

void nom_aleatoire(char *nom, int x, int y) {
	char c = 65 + rand()%26;
	nom[0] = (char)(((int)'0')+x/100);
	nom[1] = (char)(((int)'0')+x%100/10);
	nom[2] = (char)(((int)'0')+x%10);
	nom[3] = c;
	nom[4] = (char)(((int)'0')+y/100);
	nom[5] = (char)(((int)'0')+y%100/10);
	nom[6] = (char)(((int)'0')+y%10);
	nom[7] = '\0';
}

void creer_ville_alea(Ville *ville, int borne_inf, int borne_sup) {;
	char nom[9];
	int x, y;

	int_alea(&x, borne_inf, borne_sup);
	int_alea(&y, borne_inf, borne_sup);

	nom_aleatoire(nom, x, y);

	creer_ville(ville, nom, x, y);
}
