/*	HEADERS	*/
	#include <stdlib.h>
	#include <stdio.h>
	#include <time.h>
	#include <getopt.h>

	#include <MLV/MLV_all.h>

	#include "../include/interface.h"
	#include "../include/glouton.h"
	#include "../include/population.h"

int main(int argc, char *argv[]){
	Carte carte;
	FILE *resultat = NULL;
	FILE *import = NULL;
	Coordonnee coord;
	Coordonnee souris;
	coord.x = LARG;
	coord.y = LARG;
	Visite meilleur;
	int c, nb_visites;
	int nb_villes = 0;
	int t_drapeau = -1;
	int i_drapeau = -1;
	int c_drapeau = -1;
	int x_drapeau = -1;
	int e_drapeau = -1;
	int g_drapeau = -1;
	srand(time(NULL));

	if(argc < 3){;
		exit(EXIT_FAILURE);
	}
	resultat = fopen(argv[1], "w");

	while ((c = getopt(argc , argv, "vcxeg:i:t:")) != -1) {
		switch(c) {
			case 't':
				if(i_drapeau != -1) {
					printf("Vous ne pouvez demander une carte aléatoire et en importer une autre !\n");
					exit(EXIT_FAILURE);
				}
				if(c_drapeau != -1) {
					printf("Vous ne pouvez demander une carte aléatoire et de créer une autre manuellement !\n");
					exit(EXIT_FAILURE);
				}
				if(t_drapeau != -1) {
					printf("Répétition d'argument : [-t]\n");
					exit(EXIT_FAILURE);
				}
				t_drapeau = 1;
				if((nb_villes = atoi(optarg)) < 2) {
					printf("Erreur argument option [-t]\n");
					exit(EXIT_FAILURE);
				}
				break;

			case 'i':
				if(t_drapeau != -1) {
					printf("Vous ne pouvez demander une carte aléatoire et en importer une autre !\n");
					exit(EXIT_FAILURE);
				}
				if(c_drapeau != -1) {
					printf("Vous ne pouvez pas importer une carte et créer une autre manuellement !\n");
					exit(EXIT_FAILURE);
				}
				if(i_drapeau != -1) {
					printf("Répétition d'argument : [-i]\n");
					exit(EXIT_FAILURE);
				}
				import = fopen(optarg, "r");
				i_drapeau = 1;
				break;

			case 'c':
				if(t_drapeau != -1) {
					printf("Vous ne pouvez demander une carte aléatoire et de créer une autre manuellement !\n");
					exit(EXIT_FAILURE);
				}
				if(i_drapeau != -1) {
					printf("Vous ne pouvez pas importer une carte et créer une autre manuellement !\n");
					exit(EXIT_FAILURE);
				}
				if(c_drapeau != -1) {
					printf("Répétition d'argument : [-c]\n");
					exit(EXIT_FAILURE);
				}
				c_drapeau = 1;
				break;

			case 'x':
				if(e_drapeau != -1) {
					printf("Veuillez choisir entre la méthode Glouton ou Génétique\n");
					exit(EXIT_FAILURE);
				}
				if(x_drapeau != -1) {
					printf("Répétition d'argument : [-g]\n");
					exit(EXIT_FAILURE);
				}
				x_drapeau = 1;
				break;

			case 'e':
				if(x_drapeau != -1) {
					printf("Veuillez choisir entre la méthode Glouton ou Génétique\n");
					exit(EXIT_FAILURE);
				}
				if(e_drapeau != -1) {
					printf("Répétition d'argument : [-e]\n");
					exit(EXIT_FAILURE);
				}
				e_drapeau = 1;
				break;
			
			case 'g':
				if(g_drapeau != -1) {
					printf("Répétition d'argument : [-g]\n");
					exit(EXIT_FAILURE);
				}
				nb_visites = atoi(optarg);
				if(nb_visites < 4){
					printf("Erreur argument [-g]\n");
					exit(EXIT_FAILURE);
				}
				g_drapeau = 1;
				break;
		}	
	}
	creer_fenetre(&coord);
	if(c_drapeau == 1) {
		int indice_carte = -1;
		char nom[9];
		while(MLV_get_keyboard_state(MLV_KEYBOARD_RETURN) != MLV_PRESSED) {
			if(MLV_get_mouse_button_state(MLV_BUTTON_LEFT) == MLV_PRESSED) {
				MLV_get_mouse_position(&(souris.x), &(souris.y));
				MLV_wait_milliseconds(100);
				if((souris.x >= BORD && souris.x <= LARG-BORD) && ((souris.y >= BORD && souris.y <= LARG-BORD))){
					indice_carte++;
					if(indice_carte == 0) {
						init_carte(&carte, 1);
						nom_aleatoire(carte.villes[indice_carte].nom, souris.x, souris.y);
						carte.villes[indice_carte].pos_x = souris.x;
						carte.villes[indice_carte].pos_y = souris.y;
					}
					else {
						Ville ville;
						nom_aleatoire(nom, souris.x, souris.y);
						creer_ville(&ville, nom, souris.x, souris.y);
						ajouter_ville(&carte, &ville);
						strcpy(nom, "");
					}
					affiche_villes(&carte);
				}
			}
		}
	}

	else if(t_drapeau == 1) {
		creer_carte_alea(&carte, nb_villes, BORD, LARG);
	}
	else if(i_drapeau == 1) {
		importer_fichier_carte(import, &carte);
		fclose(import);
	}
	else {
		printf("Fatal Error\n");
		exit(EXIT_FAILURE);
	}

	if((x_drapeau == -1 && e_drapeau == -1) || x_drapeau == 1) {
		glouton(&carte, &meilleur);
	}
	else {
		Evolution evo;
		Population pop;
		if(g_drapeau == 1){
			init_evolution(&evo, nb_visites, 5, nb_visites/4, nb_visites/4, nb_visites/2);
		}
		else
			init_evolution(&evo, 20, 5, 5, 5, 10);
		init_population(&pop, &evo, &carte);
		MLV_wait_milliseconds(200);

		while(MLV_get_keyboard_state(MLV_KEYBOARD_RETURN) != MLV_PRESSED) {
			afficher_visite(&(pop.visites[0]), &carte);
			lancer_simulation(&pop);
			MLV_wait_milliseconds(100);
		}
		copier_visite(&meilleur, &(pop.visites[0]));
	}

	afficher_visite(&meilleur, &carte);;
	exporter_fichier_visite(resultat, &meilleur);
	supprimer_carte(&carte);
	supprimer_visite(&meilleur);
	fclose(resultat);
	MLV_wait_seconds(5);
	MLV_free_window();
	return 0;
}
