CC = colorgcc
CFLAGS = -pedantic -Wall -Werror -Wextra -std=c99 -O2
LDFLAGS = -lm -lMLV
BIN = pvc
SRC = ville.c carte.c visite.c interface.c glouton.c population.c main.c
OBJS := $(SRC:.c=.o)
OBJS :=$(addprefix src/, $(OBJS))

all: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(BIN) $(LDFLAGS)

clean:
	$(RM) $(OBJS) $(BIN)
